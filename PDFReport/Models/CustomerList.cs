﻿using System.Collections.Generic;

namespace PDFReport.Models
{
    public class CustomerList : List<Customer>
    {
        public string LogoUrl { get; set; }
        public string ImageUrl { get; set; }
    }
}