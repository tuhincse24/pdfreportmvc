﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PDFReport.Startup))]
namespace PDFReport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
